# Quantum Policy Gradient Algorithm with Optimized Action Decoding
The python toolbox implementing the methods described in [Meyer et al., "Quantum Policy Gradient Algorithm with Optimized Action Decoding", Proceedings of the 40th International Conference on Machine Learning, PMLR 202:24592-24613 (2023)](https://proceedings.mlr.press/v202/meyer23a.html)
has been moved to [this Github repository](https://github.com/nicomeyer96/quantum-policy-gradients).
